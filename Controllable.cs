﻿using Sandbox.ModAPI.Ingame;
using System.Collections.Generic;

namespace IngameScript
{
    internal partial class Program
    {
        public abstract class Controllable<T> where T : IMyTerminalBlock
        {
            protected readonly T Block;
            protected bool UpdatedConfig;
            protected readonly List<SortedList<MoveFlag, float>> Commands = new List<SortedList<MoveFlag, float>>();

            public Controllable(T block)
            {
                Block = block;
            }

            public abstract void Actuate(MoveFlag flag, int profile, float multiplier);

            protected float GetImpulse(MoveFlag flag, int profile, float multiplier)
            {
                float impulse = 0;
                foreach (var possibleFlag in MoveFlagStrings.Keys)
                {
                    if ((flag & possibleFlag) != 0)
                    {
                        float value;
                        if (Commands[profile].TryGetValue(possibleFlag, out value))
                        {
                            impulse += value * multiplier;
                        }
                    }
                }

                return impulse;
            }

            protected void ParseConfig(List<string> sections, string header)
            {
                ini.Clear();
                if (Block.CustomData.Length > 0 && ini.TryParse(Block.CustomData))
                {
                    foreach (string section in sections)
                    {
                        if (ini.ContainsSection(section))
                        {
                            var cmds = new SortedList<MoveFlag, float>(Commands.Count);
                            foreach (var cmd in MoveFlagStrings)
                            {
                                cmds.Add(cmd.Key, (float)ini.Get(section, cmd.Value).ToDouble());
                            }
                            Commands.Add(cmds);
                        }
                        else
                        {
                            SetIniDefault(section, header);
                        }
                    }
                }
                else
                {
                    foreach (string section in sections)
                    {
                        SetIniDefault(section, header);
                    }
                    SaveIni();
                }
            }

            public void SetIniDefault(string section, string header)
            {
                foreach (var cmd in MoveFlagStrings)
                {
                    ini.Set(section, cmd.Value, 0);
                }
                ini.SetSectionComment(section, header);

                UpdatedConfig = true;
            }

            public void SaveIni()
            {
                if (UpdatedConfig)
                {
                    Block.CustomData = ini.ToString();
                    ini.Clear();
                }
            }
        }
    }
}
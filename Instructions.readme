﻿## Instructions for use:

1. Create a group called "PARK" containing all the ship controllers, motors, and pistons you want this script to control.
You can change the name of the group at the beginning of the script.

2. Compile this script.
It may give some warnings if the ship controllers control thrusters or tires, which will not prevent the script from running,
but could be risky depending on your build.

3. Go into the customData of the rotors and pistons. You will see this or something similar:

;Provide the RPM of the part when the corresponding key is pressed.
[PARK:Main]
Forward=0
Backward=0
Left=0
Right=0
Up=-1
Down=1
Roll Left=0
Roll Right=0
Pitch Up=0
Pitch Down=0
Yaw Left=0
Yaw Right=0

Each value under the section under "[PARK:Main]" is a direction input and a corresponding velocity value.
For example, if you want your part to move forward and back when you press the forward and back keys,
use "Forward=5" and "Backward=-5".
Keep in mind the sign you should use is going to depend on your build.
5 and -5 represent 5RPM and 5m/s on their respective motor and piston.
The system is additive. If you have multiple keys pressed down which effect the part in different ways,
the effect is summed up and it uses total velocity.

4. Set the blocks to the values you want, and recompile to get the script to recognize changes.

5. Hop into your ship controller, test it out, and tweak as neccessary.


## Advanced: Profiles.

Look in the customData of the PB. You will find "Main" on one line by itself. This is the automatically generated profile it
refers to when first compiled. A profile defines which set of instructions to use. You can set up multiple profiles to control
multiple sets of joints on a crane arm, or use finer controls, etc.

### To set up a profile:

1) go in the PB's customData and enter the name of the new profile on a newline and recompile.
The PB will automatically take the new profile and generate a new entry similar to the one shown above.

2) Set up as neccessary.


## Commands:

Commands are case insensitive (for now) and control how the script operates.

"enable" enables the script to take commands from the ship controller.
"disable" disables the script from taking commands from the ship controller.
"toggle" toggles the enabled state on and off.
"mult <ratio>" sets the multiplier that effects the speed of configured parts. "mult 2" would double speed, for example.

Anything else will be regarded as a command to switch profiles. For example, if you have a profile called "Wrist",
you can tell the script to respond according to that profile with "wrist".

Note: These settings are saved and the script will not reset after a load.
﻿using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using System.Collections.Generic;
using VRageMath;

namespace IngameScript
{
    internal partial class Program
    {
        public class Suspension : Controllable<IMyMotorSuspension>
        {
            public Suspension(IMyMotorSuspension suspension, List<string> sections) : base(suspension)
            {
                ParseConfig(sections, "Provide the throttle of the part when the corresponding key is pressed.");
            }
            public override void Actuate(MoveFlag flag, int profile, float multiplier)
            {
                if (flag == 0)
                {
                    Block.SetValueFloat("Propulsion override", 0);
                    return;
                }

                float impulse = GetImpulse(flag, profile, multiplier); // The movement impulse of the motor. It is additive.
                Block.SetValueFloat("Propulsion override", MathHelper.Clamp(impulse * multiplier, -1, 1));
            }
        }
    }
}

﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        private string groupName = "PARK"; // Change this to change the name of the group.
        private ushort maxRampTicks = 1; // The number of ticks it will take to ramp up to full speed. Set this to a minimum of 1 to ignore.
        private string messageTag = string.Empty; // The IGC antenna message tag.
        private readonly SortedList<string, Action> ProgramCommands = new SortedList<string, Action>(StringComparer.OrdinalIgnoreCase);

        private static readonly char[] profileSeperator = new char[] { '\n' };

        private const string remoteOnline = "PARK_Online";
        private const string remoteOffline = "PARK_Offline";
        private const string remoteRegister = "PARK_Listening";
        private const string remoteCommand = "PARK_Command";

        private bool IsRemotingEnabled => !string.IsNullOrWhiteSpace(messageTag);
        private bool IsRemotePrimary => listeningToController == 0;
        private bool IsRemoteSecondary => listeningToController != 0;

        public void ParseProgramConfig()
        {
            // Initialize config from the local INI data.
            ini.Clear();
            if (Me.CustomData.Length > 0 && ini.TryParse(Me.CustomData) && ini.ContainsSection("PARK"))
            {
                if (!ini.Get("PARK", "RampTicks").TryGetUInt16(out maxRampTicks))
                {
                    if (maxRampTicks < 1)
                    {
                        Echo("Warning: Any value below 1 for RampTicks will be ignored");
                        maxRampTicks = 1;
                    }
                }
                if (!ini.Get("PARK", "GroupName").TryGetString(out groupName))
                {
                    groupName = "PARK";
                }
                if (!ini.Get("PARKRemote", "ChannelName").TryGetString(out messageTag))
                {
                    messageTag = string.Empty;
                }
            }
            else
            {
                ini.Set("PARK", "RampTicks", 3);
                ini.Set("PARK", "GroupName", "PARK");
                ini.SetSectionComment("PARK", "Change RampTicks to modify the number of script ticks movements ramp up/down over. Removing RampTicks or changing to 1 disables ramping. Change GroupName to modify the name of the group that will be used to search for controllable objects.");

                ini.Set("PARKRemote", "ChannelName", string.Empty);
                ini.SetSectionComment("PARKRemote", "To enable multiple unconnected grids to control each other via PARK, change the channel name to unique value that is shared between all grids that can control each other, e.g. \"Gantry Crane\". The channel name for all grids that participate must exactly match.");

                ini.SetEndComment("Put the list of profiles, one per line, below the line that is only '---'. Do not remove the --- as this marks the end of the configuration and the beginning of the profile list.");

                if (Me.CustomData.Length > 0)
                {
                    ini.EndContent = Me.CustomData; // Handle upgrade case from older PARK so we don't clobber profiles.
                }
                else
                {
                    ini.EndContent = "Main";
                }

                Me.CustomData = ini.ToString();
            }

            foreach (string value in ini.EndContent.Split(profileSeperator, options: StringSplitOptions.RemoveEmptyEntries))
            {
                profiles.Add(value);
                sections.Add($"PARK:{value}");
            }

            // Initialize commands and profiles
            ProgramCommands.Add("enable", () => enabled = true);
            ProgramCommands.Add("disable", () => enabled = false);
            ProgramCommands.Add("toggle", () => enabled = !enabled);
            ProgramCommands.Add("mult", () =>
            {
                float m;
                if (commandLine.Items.Count > 1 && float.TryParse(commandLine.Items[1], out m) && m > 0)
                {
                    multiplier = m;
                }
                else
                {
                    Echo("Warning: missing or invalid (not a number) multiplier. The multiplier must be a whole or decimal number greater than 0.0");
                }
            });

            for (int i = 0; i < profiles.Count; i++)
            {
                var index = i; // Capture the variable so it doesn't refer back to the loop index
                ProgramCommands.Add(profiles[index], () => profile = index);
            }
            ini.SetEndComment(null);// Clearing the INI doesn't clear the endcomment
            ini.Clear();
        }
    }
}

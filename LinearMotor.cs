﻿using Sandbox.ModAPI.Ingame;
using System.Collections.Generic;
using VRageMath;

namespace IngameScript
{
    internal partial class Program
    {
        public class LinearMotor : Controllable<IMyPistonBase>
        {
            public LinearMotor(IMyPistonBase linearMotor, List<string> sections) : base(linearMotor)
            {
                ParseConfig(sections, "Provide the M/S of the part when the corresponding key is pressed.");
            }
            public override void Actuate(MoveFlag flag, int profile, float multiplier)
            {
                if (flag == 0)
                {
                    Block.Velocity = 0;
                    return;
                }

                float impulse = GetImpulse(flag, profile, multiplier); // The movement impulse of the motor. It is additive.
                Block.Velocity = MathHelper.Clamp(impulse * multiplier, -Block.MaxVelocity, Block.MaxVelocity);
            }
        }
    }
}

﻿using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using System.Collections.Generic;
using VRageMath;

namespace IngameScript
{
    internal partial class Program
    {
        public class Motor : Controllable<IMyMotorStator>
        {
            private readonly float maxVel;
            public Motor(IMyMotorStator motor, List<string> sections) : base(motor)
            {
                ParseConfig(sections, "Provide the RPM of the part when the corresponding key is pressed.");
                maxVel = Block.GetMaximum<float>("Velocity") * MathHelper.RadiansPerSecondToRPM;
            }

            public override void Actuate(MoveFlag flag, int profile, float multiplier)
            {
                if (flag == 0)
                {
                    Block.TargetVelocityRPM = 0;
                    return;
                }

                float impulse = GetImpulse(flag, profile, multiplier); // The movement impulse of the motor. It is additive.
                Block.TargetVelocityRPM = MathHelper.Clamp(impulse * multiplier, -maxVel, maxVel);
            }
        }
    }
}

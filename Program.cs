﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using System.Linq;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRageMath;

namespace IngameScript
{
    internal partial class Program : MyGridProgram
    {
        #region mdk preserve

        const string VER = "v1.2.0";
        #endregion

        private int counter = 0;
        private double runtimeMS;
        private bool enabled = true;
        private string enableStr = "";
        private float multiplier = 1;
        private int rampTicks = 1;
        private bool isUnderLocalControl = false;

        private static readonly MyIni ini = new MyIni();
        private static readonly MyCommandLine commandLine = new MyCommandLine();

        public static readonly SortedList<MoveFlag, string> MoveFlagStrings = new SortedList<MoveFlag, string> {
                { MoveFlag.Forward, "Forward" },
                { MoveFlag.Backward, "Backward" },
                { MoveFlag.StrafeLeft, "Left" },
                { MoveFlag.StrafeRight, "Right" },
                { MoveFlag.Up, "Up" },
                { MoveFlag.Down, "Down" },
                { MoveFlag.RollLeft, "Roll Left" },
                { MoveFlag.RollRight, "Roll Right" },
                { MoveFlag.PitchUp, "Pitch Up" },
                { MoveFlag.PitchDown, "Pitch Down" },
                { MoveFlag.YawLeft, "Yaw Left" },
                { MoveFlag.YawRight, "Yaw Right" } };
        private static readonly List<string> currentMovements = new List<string>(MoveFlagStrings.Count);

        private readonly List<IMyShipController> controllers = new List<IMyShipController>();
        private readonly List<Motor> motors = new List<Motor>();
        private readonly List<LinearMotor> linearMotors = new List<LinearMotor>();
        private readonly List<Suspension> suspensions = new List<Suspension>();
        private readonly List<string> profiles = new List<string>();
        private int profile = 0;
        private readonly List<string> sections = new List<string>();

        private readonly IMyBroadcastListener broadcastListener;
        private readonly HashSet<long> remoteControllers = new HashSet<long>();
        private long listeningToController = 0;

        private MoveFlag flag = MoveFlag.None;// The flag indicating the binary movement values.
        private MoveFlag prev = MoveFlag.None;// The previous flag state. Used to prevent the same button press from activating multiple times.

        public string PrettyString(MoveFlag moveFlag)
        {
            if (moveFlag == 0)
            {
                return "None";
            }

            currentMovements.Clear();
            foreach (var mfs in MoveFlagStrings)
            {
                if ((moveFlag & mfs.Key) != 0)
                {
                    currentMovements.Add(mfs.Value);
                }
            }
            return string.Join(" | ", currentMovements);
        }

        private bool InitBlock<T>(T block) where T : IMyTerminalBlock
        {
            IMyMotorStator motor = block as IMyMotorStator;
            if (motor != null)
            {
                motors.Add(new Motor(motor, sections));
                return false;
            }

            IMyPistonBase piston = block as IMyPistonBase;
            if (piston != null)
            {
                linearMotors.Add(new LinearMotor(piston, sections));
                return false;
            }

            IMyMotorSuspension suspension = block as IMyMotorSuspension;
            if (suspension != null)
            {
                suspensions.Add(new Suspension(suspension, sections));
                return false;
            }

            IMyShipController controller = block as IMyShipController;
            if (controller != null)
            {
                controllers.Add(controller);
                if (!controller.IsWorking)
                    Echo($"Warning: controller \"{controller.CustomName}\" is not functional.");
                if (controller.IsUnderControl)
                    Echo($"Info: Already controlling {controller.CustomName}.");
                if (controller.ControlThrusters)
                    Echo($"Warning: controller \"{controller.CustomName}\" is set to control thrusters.");
                if (controller.ControlWheels && controller.HasWheels)
                    Echo($"Warning: controller \"{controller.CustomName}\" is set to control wheels.");
                return false;
            }

            return false;
        }

        private int rI = -1;
        private readonly double[] reading = new double[30];

        private double AvgRuntimeMs()
        {
            if (++rI >= reading.Length)
                rI = 0;
            reading[rI] = Runtime.LastRunTimeMs;
            return reading.Average();
        }

        private const float SENS = 0.35355f;

        public Program()
        {
            IMyBlockGroup group = GridTerminalSystem.GetBlockGroupWithName(groupName);

            if (group == null)
            {
                throw new InvalidOperationException($"There is no group with the name \"{groupName}\".\nEnsure you have all blocks you wish to control under this group and recompile.");
            }

            ParseProgramConfig();

            broadcastListener = IGC.RegisterBroadcastListener(messageTag);
            broadcastListener.SetMessageCallback(messageTag);
            IGC.UnicastListener.SetMessageCallback(messageTag);

            group.GetBlocksOfType<IMyTerminalBlock>(null, b => InitBlock(b));

            if (controllers.Count == 0)
            {
                throw new InvalidOperationException($"There are no controllers in \"{groupName}\".\nEnsure you have at least one controller under this group and recompile.");
            }
            if (motors.Count == 0 && linearMotors.Count == 0 && suspensions.Count == 0)
            {
                throw new InvalidOperationException($"There are no rotors, pistons, or wheel suspensions in \"{groupName}\".\nEnsure you have at least one rotor, piston, or wheel suspension under this group and recompile.");
            }

            if (Storage.Length > 0)
            {
                ini.Clear();
                ini.TryParse(Storage);

                enabled = ini.Get("PARK", "Enabled").ToBoolean();

                profile = Math.Max(0, profiles.FindIndex(i => i.Equals(ini.Get("PARK", "Current Profile").ToString(), StringComparison.OrdinalIgnoreCase)));

                if (ini.ContainsKey("PARK", "Multiplier"))
                    multiplier = (float)ini.Get("PARK", "Multiplier").ToDouble();
            }

            if (Me.SurfaceCount > 0)
            {
                Me.GetSurface(0).ContentType = ContentType.TEXT_AND_IMAGE;
            }
            else
            {
                Echo($"There is no display on this PB. Text output disabled.");
            }

            Runtime.UpdateFrequency = UpdateFrequency.Update10;
        }

        public void Save()
        {
            Storage = "";
            ini.Clear();
            ini.Set("PARK", "Enabled", enabled);
            ini.Set("PARK", "Current Profile", profiles[profile]);
            Storage = ini.ToString();
            ini.Clear();
        }

        public void Main(string argument, UpdateType updateSource)
        {
            switch (updateSource)
            {
                case UpdateType.Terminal:
                case UpdateType.Trigger:
                case UpdateType.Script:
                case UpdateType.Mod:
                    HandleManualActivation(argument);
                    return;
                case UpdateType.Update1:
                    Runtime.UpdateFrequency = UpdateFrequency.Update10;
                    return;
                case UpdateType.Update10:
                case UpdateType.Update100:
                case UpdateType.Once:
                    HandleTickActivation();
                    break;
                case UpdateType.IGC:
                    HandleIgcActivation();
                    break;
                case UpdateType.None:
                default:
                    Echo("Unknown activation type, ignoring");
                    break;
            }

            if (!(IsRemoteSecondary && updateSource == UpdateType.IGC))
            {
                if (flag != prev) // Filter to ensure the script only sends updates to the blocks when the input changes.
                {
                    rampTicks = maxRampTicks;
                }

                if (rampTicks != 0)
                {
                    if (motors.Count > 0)
                    {
                        foreach (Motor motor in motors)
                            motor.Actuate(flag, profile, multiplier / rampTicks);
                    }
                    if (linearMotors.Count > 0)
                    {
                        foreach (LinearMotor linearMotor in linearMotors)
                            linearMotor.Actuate(flag, profile, multiplier / rampTicks);
                    }
                    if (suspensions.Count > 0)
                    {
                        foreach (Suspension suspension in suspensions)
                            suspension.Actuate(flag, profile, multiplier);
                    }
                    rampTicks--;
                    prev = flag;
                    foreach (var remoteEndpoint in remoteControllers)
                    {
                        IGC.SendUnicastMessage(remoteEndpoint, messageTag, (ushort)flag);
                    }
                }

                runtimeMS = AvgRuntimeMs();

                if (Me.SurfaceCount > 0 && counter++ >= 6)
                {
                    counter = 0;
                    enableStr = enabled ? "Enabled" : "Disabled";
                    Me.GetSurface(0).WriteText($"Piston And Rotor Keybinds {VER}\nRuntime: {runtimeMS:0.000}ms\nCurrent Profile: {profiles[profile]}\n{enableStr}\nKeyPresses: {PrettyString(flag)}");
                }
            }
        }

        private void HandleIgcActivation()
        {
            if (!IsRemotingEnabled)
            {
                return;
            }
            while (broadcastListener.HasPendingMessage)
            {
                var message = broadcastListener.AcceptMessage();
                if (message.Tag == messageTag)
                {
                    if (message.Data is string)
                    {
                        var data = (string)message.Data;
                        if (data == remoteOnline && !IsRemoteSecondary)
                        {
                            listeningToController = message.Source;
                            Runtime.UpdateFrequency = UpdateFrequency.Update1;
                            IGC.SendUnicastMessage(message.Source, messageTag, $"{remoteRegister}");
                        }
                        else if (IsRemoteSecondary && data == remoteOffline && message.Source == listeningToController)
                        {
                            listeningToController = 0;
                        }
                    }
                    else
                    {
                        Echo("Warning: Unknown IGC message data type on broadcast channel, ignoring");
                    }
                }
            }

            while (IGC.UnicastListener.HasPendingMessage)
            {
                var message = IGC.UnicastListener.AcceptMessage();
                if (message.Tag == messageTag)
                {
                    if (message.Data is string)
                    {
                        var data = (string)message.Data;
                        if (data == remoteRegister && IsRemotePrimary)
                        {
                            remoteControllers.Add(message.Source);
                            IGC.SendUnicastMessage(message.Source, messageTag, (ushort)flag);
                        }
                        if (data.StartsWith(remoteCommand) && IsRemoteSecondary)
                        {
                            HandleManualActivation(data.Substring(remoteCommand.Length + 1));
                        }
                    }
                    else
                    {
                        if (message.Tag == messageTag &&
                            enabled &&
                            message.Source == listeningToController &&
                            message.Data is ushort)
                        {
                            flag = (MoveFlag)message.Data;
                        }
                    }
                }
            }
        }

        private void HandleTickActivation()
        {
            var prevLocalControl = isUnderLocalControl;
            if (enabled) // Do not read cockpit information if the script is disabled. This will cause the script to stop movement.
            {
                isUnderLocalControl = false;

                foreach (IMyShipController controller in controllers)
                {
                    if (controller.IsWorking && controller.IsUnderControl)
                    {
                        isUnderLocalControl = true;

                        Vector3D vector = Vector3D.Normalize(controller.MoveIndicator);
                        Vector2 rotation = Vector2.Normalize(controller.RotationIndicator);

                        flag = MoveFlag.None;// Nothing has been pressed.

                        if (vector.Z < -SENS)// Forward
                            flag |= MoveFlag.Forward;
                        else if (vector.Z > SENS)// Backward
                            flag |= MoveFlag.Backward;

                        if (vector.X < -SENS)// Left
                            flag |= MoveFlag.StrafeLeft;
                        else if (vector.X > SENS)// Right
                            flag |= MoveFlag.StrafeRight;

                        if (vector.Y > SENS)// Jump
                            flag |= MoveFlag.Up;
                        else if (vector.Y < -SENS)// Crouch
                            flag |= MoveFlag.Down;

                        if (controller.RollIndicator < -SENS)// Roll Left
                            flag |= MoveFlag.RollLeft;
                        else if (controller.RollIndicator > SENS)// Roll Right
                            flag |= MoveFlag.RollRight;

                        if (rotation.X > SENS)// Pitch Down
                            flag |= MoveFlag.PitchDown;
                        else if (rotation.X < -SENS)// Pitch Up
                            flag |= MoveFlag.PitchUp;

                        if (rotation.Y < -SENS)// Yaw Left
                            flag |= MoveFlag.YawLeft;
                        else if (rotation.Y > SENS)// Yaw Right
                            flag |= MoveFlag.YawRight;
                    }
                }
            }

            if (!enabled || (!isUnderLocalControl && IsRemotePrimary))
            {
                flag = MoveFlag.None;
            }

            if (IsRemotingEnabled && IsRemotePrimary)
            {
                // Let everyone know control is established locally and they should start listening/moving
                if (!prevLocalControl && isUnderLocalControl)
                {
                    IGC.SendBroadcastMessage(messageTag, remoteOnline);
                    remoteControllers.Clear();
                }
                else if (prevLocalControl && !isUnderLocalControl) // Let everyone know to stop listening
                {
                    IGC.SendBroadcastMessage(messageTag, remoteOffline);
                    remoteControllers.Clear();
                }
            }
        }

        private void HandleManualActivation(string argument)
        {
            commandLine.Clear();

            if (argument.Length > 0 && commandLine.TryParse(argument))
            {
                Action commandAction;
                string command = commandLine.Argument(0);

                if (ProgramCommands.TryGetValue(command, out commandAction))
                {
                    commandAction();

                    if (IsRemotingEnabled && IsRemotePrimary)
                    {
                        foreach (var remoteEndpoint in remoteControllers)
                        {
                            IGC.SendUnicastMessage(remoteEndpoint, messageTag, $"{remoteCommand}:{argument}");
                        }
                    }
                }
                else
                {
                    Echo($"Warning: Unknown profile or command '{command}'");
                }
            }

            commandLine.Clear();
        }
    }
}

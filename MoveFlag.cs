﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System;
using VRage.Collections;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game;
using VRage;
using VRageMath;

namespace IngameScript
{
    partial class Program
    {
        [Flags]
        public enum MoveFlag : ushort
        {
            None = 0,
            Forward = 1,
            Backward = 1 << 1,
            StrafeLeft = 1 << 2,
            StrafeRight = 1 << 3,
            Up = 1 << 4,
            Down = 1 << 5,
            RollLeft = 1 << 6,
            RollRight = 1 << 7,
            PitchUp = 1 << 8,
            PitchDown = 1 << 9,
            YawLeft = 1 << 10,
            YawRight = 1 << 11,
        }
    }
}
